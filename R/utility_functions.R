#' Confluent Hypergeometric Function of the first kind.
#'
#' This is a wrapper
#' function for kummerM() from the fAsianOptions packages
#'
#' @param a numerator falling factorial parameter
#' @param b denominatior falling factorial parameter
#' @param x function input
#'
#' @return value of F_1(a, b; x) = sum_{k=0}^{\infty} \frac{(a)_k x^k}{(b)_k k!}
#' @export
#'
#' @examples
#'
#'
F1 <- function(a,b,x){
  require("fAsianOptions")
  Re(kummerM(x = x, a = a, b = b))
}

#' Function defined in "Orthant probabilities" by Livsey, McElroy and Lund.
#'
#' @param el function input
#' @param mu mean (scalar)
#' @param sig2 variance (scalar)
#'
#' @return value of the function
#' @export
#'
#' @examples
#'
#'
g <- function(el, mu, sig2)
{
   # el = 0
   if(el==0){
      return( sqrt(2)*mu / sqrt(sig2) *
              F1((el+1)/2, 3/2, -mu^2/(sig2*2)) /
              gamma(1/2 - el/2) +
              F1(el/2, 1/2, -mu^2/(sig2*2))/ gamma(1-el/2)   )
   }else{
      # even case
      if(el%%2==0){
       return( sqrt(2)*mu / sqrt(sig2) *
               F1((el+1)/2, 3/2, -mu^2/(sig2*2)) /
               gamma(1/2 - el/2)  )
      }
      # odd case
      if(el%%2==1){
         return(F1(el/2, 1/2, -mu^2/(sig2*2))/ gamma(1-el/2))
      }
   }
}


#' produces product of g_\tilde(el_m) for m=1,2,...,n
#'
#' @param L
#' @param mu
#' @param Sig
#'
#' @return
#' @export
#'
#' @examples
g_prod = function(L, mu, Sig){
   n = length(mu)
   terms = c()
   for(i in 1:n){
      terms[i] = g(el = sum(L[i, ])+sum(L[, i]), mu = mu[i], sig2 = Sig[i,i])
   }
   return(prod(terms))
}

# returm upper triangluar elements of matrix
upTri = function(A){ A[upper.tri(A)] }

# expand.grid() function for increasing dimension d
expand.grids <- function(x,d) {
  expand.grid(replicate(d, x, simplify=FALSE))
}

# fill the upper triangle of a (d x d) matrix with the vector v
fill_upper_tri = function(v, d){
  if(length(v) != choose(d,2)) stop("dimension error: length(v) != choose(d,2)")
   b = matrix(0, d, d)
  b[lower.tri(b, diag=FALSE)] <- v
  b <- t(b)
  return(b)
}

# function print progress in a for loop
print_progress = function(i, total.iter, print.every=1000){
   if(i==1) cat("1 out of ", total.iter, "\n")
   if(i%%print.every == 0 ) cat(i, "out of ", total.iter, ":  ", round(i/total.iter*100), "% done", "\n")
}

# Use dec2base(x, n) for converting x to base n. From oro.dicom package



