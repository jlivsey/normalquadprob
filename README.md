# README #

This R package calculates quadrant probabilities of the multivariate normal distribution.

### To do list ###

* set up NAMESPACE file with required packages
* Investigate poor convergence when rho \neq 0
